
INTRODUCTION 

This project is helpful to testing remote tests-jars from Gradle.Basically project can have different modules.
Each module can be done by different people in a team.But may or may not  Modules are dependent on another modules in some projects.
At that time what we can do is we can create executable jar for each module and we can share with  the team by uploading these jars in some remote repository.Another members in a team can use that jars and execute the project based on their requirement.
At the time of  building if we will get any exception in the project then the build process will be stop. At this time difficult to identify the exception in the project.

OBJECTIVE


The objective of this project is to provide facility for testing before using remote jars to find weather their code is perfect or not. Developers of particular module/project, They need to provide the tests-jar including executable jars to do testing. Developers who are using remote jar, they need to take  tests-jar(if available) also and they need to test before use.

SOFTWARE

1.Java

2.Gradle(Building Tool Like Ant and Maven)



TESTING REMOTE TEST-JARS FROM GRADLE

Step 1:

configurations
 {
            testsFromJar {
                transitive = false
            }
            junitAnt
 }

Step 2:

dependencies

 {
        junitAnt('org.apache.ant:ant-junit:1.9.3') {
            transitive = false
        }
        junitAnt('org.apache.ant:ant-junit4:1.9.3') {
            transitive = false
        }
    compile "groupid:artifactname:version"
    compile "groupid:artifactname:version:tests"
    testsFromJar ( group:'groupid', name:'artifactname', version:'version',classifier:'tests')
 }

Step 3:

ant
 {
    taskdef(name: 'junit', classname: 'org.apache.tools.ant.taskdefs.optional.junit.JUnitTask',
            classpath: configurations.junitAnt.asPath)
 }

Step 4: 


task  runTestsFromJar(  ) << 

 {
        configurations.testsFromJar.each {
            file ->
                ant.junit(printsummary:'on', fork:'yes', showoutput:'yes', haltonfailure:'yes') { //configure junit task as per your need
                    formatter (type:'xml')
                    batchtest(todir:"$temporaryDir", skipNonTests:'true' ) {
                        zipfileset(src:file,
                                includes:"**/*Test.class",
                        )
                    }
                    classpath {
                        fileset(file:file)
                        pathelement(path:sourceSets.main.compileClasspath.asPath+sourceSets.test.compileClasspath.asPath)
                    }
                }
        }
 }
 
 
 
includes:"**/*Test.class" ( Its only to test which classname end with "Test" )

includes:"**/*IntegrationTest.class" (If we wants to run only integrationtests then we can identify the integration tests by giving "IntegrationTest" to last of the classname.)

includes:"**/*UnitTest.class" (To test only Unit Tests)

By default all tests will be run.



 

Step 5:

gradle  runTestsFromJar


Finally test results will be stored in specified directory and with specified format. Here I am storing test results in "temporaryDir" under Build folder with "xml" format


