# INTRODUCTION#

This application is helpful for testing remote tests-jars from Gradle. Projects will be having multiple modules. Multiple people will work on each module in a team. Each and every module may or may not depend on each other.

We will create executable jar for each and every module and share the same to team by maintaining them in some remote repository. So, that every one in the team can access the required jars based on their requirement.

At the time of building if we will get any exception in the project then the build process will be stop. Here difficult to identify the exception in the project because we are using remote jars also.
### OBJECTIVE ###

The objective of this project is to provide facility for testing before using remote jars to find weather their code is perfect or not. 

Developers of particular module/project, They need to provide the tests-jar including executable jars to do testing. Developers who are using remote jar, they need to take  tests-jar(if available) also and they need to test before use.

### SOFTWARE ###

* Java
* Gradle (Building Tool Like Ant and Maven)


### TESTING REMOTE TEST-JARS FROM GRADLE ###

Step 1:


```
#!java

configurations
 {
            testsFromJar {
                transitive = false
            }
            junitAnt
 }

```

Step 2:


```
#!java

dependencies

 {
        junitAnt('org.apache.ant:ant-junit:1.9.3') {
            transitive = false
        }
        junitAnt('org.apache.ant:ant-junit4:1.9.3') {
            transitive = false
        }
    compile "groupid:artifactname:version"
    compile "groupid:artifactname:version:tests"
    testsFromJar ( group:'groupid', name:'artifactname', version:'version',classifier:'tests')
 }

```

Step 3:


```
#!java

ant
 {
    taskdef(name: 'junit', classname: 'org.apache.tools.ant.taskdefs.optional.junit.JUnitTask',
            classpath: configurations.junitAnt.asPath)
 }

```

Step 4: 



```
#!java

task  runTestsFromJar(  ) << 

 {
        configurations.testsFromJar.each {
            file ->
                ant.junit(printsummary:'on', fork:'yes', showoutput:'yes', haltonfailure:'yes') { //configure junit task as per your need
                    formatter (type:'xml')
                    batchtest(todir:"$temporaryDir", skipNonTests:'true' ) {
                        zipfileset(src:file,
                                includes:"**/*Test.class",
                        )
                    }
                    classpath {
                        fileset(file:file)
                        pathelement(path:sourceSets.main.compileClasspath.asPath+sourceSets.test.compileClasspath.asPath)
                    }
                }
        }
 }

```
 
 
 

```
#!java

includes:"**/*Test.class"
```
 ( Its only to test which classname end with "Test" )


```
#!java

includes:"**/*IntegrationTest.class"
```
 (If we wants to run only integrationtests then we can identify the integration tests by giving "IntegrationTest" to last of the classname.)


```
#!java

includes:"**/*UnitTest.class" 
```
(To test only Unit Tests)

By default all tests will be run.



 

Step 5:


```
#!java

gradle  runTestsFromJar
```



Finally test results will be stored in specified directory and with specified format. Here I am storing test results in "temporaryDir" under Build folder with "xml" format